import { TestBed } from '@angular/core/testing';

import { MealStorageService } from './meal-storage.service';

describe('MealStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MealStorageService = TestBed.get(MealStorageService);
    expect(service).toBeTruthy();
  });
});
