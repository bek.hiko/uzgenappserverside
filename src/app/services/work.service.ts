import { Work } from './../models/work';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WorkService {
  private workCollection: AngularFirestoreCollection<Work>;
  private work: Observable<Work[]>
  anColl = 'workCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.workCollection = db.collection<Work>(this.anColl);
    this.work = this.workCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Work;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      animal => animal.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'workCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: Work) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(work: Work){
    const id = this.db.createId();
    work.id = id;
    const math = JSON.parse(JSON.stringify(work));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.workCollection.doc(id).delete();
  };
}
