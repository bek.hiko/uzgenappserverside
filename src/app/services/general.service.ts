import { General } from './../models/general';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  private generalCollection: AngularFirestoreCollection<General>;
  private general: Observable<General[]>
  anColl = 'generalCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.generalCollection = db.collection<General>(this.anColl);
    this.general = this.generalCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as General;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAllAnimal() {
    return this.general;
  };
  
  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      animal => animal.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'generalCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: General) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(general: General): Promise<void> {
    const id = this.db.createId();
    general.id = id;
    const math = JSON.parse(JSON.stringify(general));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.generalCollection.doc(id).delete();
  };
}
