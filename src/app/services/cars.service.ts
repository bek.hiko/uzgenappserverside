import { Cars } from './../models/cars';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
@Injectable({
  providedIn: 'root'
})
export class CarsService {
  private carsCollection: AngularFirestoreCollection<Cars>;
  private cars: Observable<Cars[]>
  anColl = 'carsCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.carsCollection = db.collection<Cars>(this.anColl);
    this.cars = this.carsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Cars;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAllAnimal() {
    return this.cars;
  };
  
  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      animal => animal.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'carsCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: Cars) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(cars: Cars): Promise<void> {
    const id = this.db.createId();
    cars.id = id;
    const math = JSON.parse(JSON.stringify(cars));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.carsCollection.doc(id).delete();
  };
}
