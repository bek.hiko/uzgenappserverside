import { Animals } from './../models/animals';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
@Injectable({
  providedIn: 'root'
})
export class AnimalsService {
  private animalsCollection: AngularFirestoreCollection<Animals>;
  private animals: Observable<Animals[]>
  anColl = 'animalsCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.animalsCollection = db.collection<Animals>(this.anColl);
    this.animals = this.animalsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Animals;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAllAnimal() {
    return this.animals;
  };
  
  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      animal => animal.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'animalsCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: Animals) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(animals: Animals){
    const id = this.db.createId();
    animals.id = id;
    const math = JSON.parse(JSON.stringify(animals));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.animalsCollection.doc(id).delete();
  };

}
