import { Electronics } from './../models/electronics';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class ElectronicsService {
  private electronicsCollection: AngularFirestoreCollection<Electronics>;
  private electronics: Observable<Electronics[]>
  anColl = 'electronicsCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.electronicsCollection = db.collection<Electronics>(this.anColl);
    this.electronics = this.electronicsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Electronics;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAllAnimal() {
    return this.electronics;
  };
  
  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      animal => animal.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'electronicsCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: Electronics) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(electronics: Electronics): Promise<void> {
    const id = this.db.createId();
    electronics.id = id;
    const math = JSON.parse(JSON.stringify(electronics));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.electronicsCollection.doc(id).delete();
  };
}
