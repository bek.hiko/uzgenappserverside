import { Taksi } from './../models/taksi';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map, take } from 'rxjs/operators';
import * as firebase from 'firebase/app';

export interface Admin {
  id?: string,
  name: string,
  surname: string,
  email: string,
  nickname:string,
  myAds?:[]

}

 @Injectable({
  providedIn: 'root'
})
export class AdminAuthService {
  taksi = new Taksi
  public isLoggedin:false;
  private adminCollection: AngularFirestoreCollection<Admin>;
  private taksiCollection: AngularFirestoreCollection<Taksi>;
  private users: Observable<Admin[]>
  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth,
    private router: Router,
  
  ) {
    this.adminCollection = db.collection<Admin>('AdminColl');
    this.taksiCollection = db.collection<Taksi>('taksiColl');
    this.users = this.adminCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  registerUser(value) {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.auth.createUserWithEmailAndPassword(value.email, value.password).then(
        res => {
        if(typeof value.carModel === 'undefined'){
          console.log("User id after reigstration = "+res.user.uid);
          let admin: Admin = {
            nickname:value.nickname,
            email: value.email,
            id: res.user.uid,
            name: value.name,
            surname:value.surname
            
          };
          this.adminCollection.doc(res.user.uid).set(admin);
          resolve(res);
        } else{
          this.taksi.carModel =value.carModel
          this.taksi.email = value.email
          this.taksi.id = res.user.uid
          this.taksi.taksiName = value.name
          this.taksi.location =value.location
          this.taksi.contactNumber =value.contactNumber,
          this.taksi.isTaksi=value.isTaksist
          
          this.taksiCollection.doc(res.user.uid).set(JSON.parse(JSON.stringify(this.taksi)));
          resolve(res);
        }
         
        }, err => {
          reject(err);
        }
      )
    })
  }

  login(value) {
    return firebase.auth().signInWithEmailAndPassword(value.email, value.password)
  }

  logout() {
    firebase.auth().signOut();
  }

  getCurrentAdminInfo(id:string){
    return this.adminCollection.doc<Admin>(id).valueChanges().pipe(
      take(1),
      map(res =>{
          res.id=id;
          return res;
      })
    )

  }
  
  getCurrentTaksiInfo(id:string){
    return this.taksiCollection.doc<Taksi>(id).valueChanges().pipe(
      take(1),
      map(res =>{
          res.id=id;
          return res;
      })
    )

  }

  AddnewAdtoMyAds(id:string){
    let myAds =[];
    this.adminCollection.doc<Admin>(id).valueChanges().pipe(
      take(1),
      map(res =>{
        myAds= res.myAds
        return res.id
        console.log("My Ads", myAds)
      })
      
    )
    // this.adminCollection.doc<Admin>(id).update({
        
    // })
  }


  deleteUserInfo(id:string){
    return this.adminCollection.doc<Admin>(id).delete();
  }

  updateTaksiInfo(id:string, work:boolean){
    return this.getById(id).update({active: work});

  }

  getById(id: string) {
    return this.db.doc<any>(`taksiColl/${id}`);
  }

}
