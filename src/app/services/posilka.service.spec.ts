import { TestBed } from '@angular/core/testing';

import { PosilkaService } from './posilka.service';

describe('PosilkaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PosilkaService = TestBed.get(PosilkaService);
    expect(service).toBeTruthy();
  });
});
