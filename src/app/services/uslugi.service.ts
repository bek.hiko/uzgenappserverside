import { Uslugi } from './../models/uslugi';
import { Work } from './../models/work';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UslugiService {
  private uslugiCollection: AngularFirestoreCollection<Uslugi>;
  private uslugi: Observable<Uslugi[]>
  anColl = 'uslugiCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.uslugiCollection = db.collection<Uslugi>(this.anColl);
    this.uslugi = this.uslugiCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Uslugi;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAll() {
    return this.uslugi;
  };
  
  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      uslugi => uslugi.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'workCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: Work) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(work: Work){
    const id = this.db.createId();
    work.id = id;
    const math = JSON.parse(JSON.stringify(work));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.uslugiCollection.doc(id).delete();
  };
}
