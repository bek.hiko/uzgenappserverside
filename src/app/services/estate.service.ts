import { Estate } from './../models/estate';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
@Injectable({
  providedIn: 'root'
})
export class EstateService {
  private estateCollection: AngularFirestoreCollection<Estate>;
  private estate: Observable<Estate[]>
  anColl = 'estateCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.estateCollection = db.collection<Estate>(this.anColl);
    this.estate = this.estateCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Estate;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAllAnimal() {
    return this.estate;
  };
  
  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      animal => animal.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'estateCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: Estate) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(estate: Estate): Promise<void> {
    const id = this.db.createId();
    estate.id = id;
    const math = JSON.parse(JSON.stringify(estate));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.estateCollection.doc(id).delete();
  };

}
