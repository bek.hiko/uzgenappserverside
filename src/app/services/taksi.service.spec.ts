import { TestBed } from '@angular/core/testing';

import { TaksiService } from './taksi.service';

describe('TaksiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaksiService = TestBed.get(TaksiService);
    expect(service).toBeTruthy();
  });
});
