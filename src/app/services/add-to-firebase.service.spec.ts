import { TestBed } from '@angular/core/testing';

import { AddToFirebaseService } from './add-to-firebase.service';

describe('AddToFirebaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddToFirebaseService = TestBed.get(AddToFirebaseService);
    expect(service).toBeTruthy();
  });
});
