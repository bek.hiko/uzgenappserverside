import { TestBed } from '@angular/core/testing';

import { UslugiService } from './uslugi.service';

describe('UslugiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UslugiService = TestBed.get(UslugiService);
    expect(service).toBeTruthy();
  });
});
