import { Restaurant } from './../models/restaurant';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';
@Injectable({
  providedIn: 'root'
})
export class RestuarantService {
  private restaurantCollection: AngularFirestoreCollection<Restaurant>;
  private restaurant: Observable<Restaurant[]>
  anColl = 'restaurantCollection';

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {
    this.restaurantCollection = db.collection<Restaurant>(this.anColl);
    this.restaurant = this.restaurantCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Restaurant;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAllAnimal() {
    return this.restaurant;
  };
  
  getMyAds(adminNick){
    return this.db.collection(this.anColl, 
      animal => animal.where('authorNick', '==', adminNick)).valueChanges();
  }
  
  getById(id: string) {
    return this.db.doc<any>(`${'restaurantCollection'}/${id}`);
  }

  getAnimalById(uid: string) {
    return this.getById(uid).valueChanges();
  }

  update(id: string, data: Restaurant) {
    // TODO: check partial update vs full update
    return this.getById(id).update(data);
  }

  addNewAnimal(restaurant: Restaurant): Promise<void> {
    const id = this.db.createId();
    restaurant.id = id;
    const math = JSON.parse(JSON.stringify(restaurant));
    return this.db.doc(`${this.anColl}/${id}`).set(math);
  }

  deleteAd(id: string): Promise<void> {
    return this.restaurantCollection.doc(id).delete();
  };

}
