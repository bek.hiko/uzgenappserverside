import { Storage } from '@ionic/storage';
import { Meal } from './../models/meal';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MealStorageService {

  mealKey = 'currentMealArray';
  
  item: any[] = [];

  constructor( private storage:Storage) {
  }

  async getMealArray():Promise<any>{
    this.storage.get(this.mealKey).then(res =>{
      if (res === null) {
        return undefined;

      }
      return (res);
    })  
    
  }

   async saveMealAsArray(meal:Meal) {
     if( await this.getMealArray() == undefined){
      this.item.push(meal);
      this.storage.set(this.mealKey,this.item)
    }else{
      this.item = await this.getMealArray();
      this.item.push(meal)
      this.storage.set(this.mealKey, this.item)
    }
    
  }

  deleteMealArray() {
    this.storage.remove(this.mealKey);
  }

}
