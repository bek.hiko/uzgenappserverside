
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase/app';


export interface Ads {
  id?: string,
  location: string,
  adminName: string,
  ownerNickname: string,
  adTopic: string,
  adType: string,
  startdate: string,
  endDate: string,
  category: string,
  description: string,
  keywords: string,
  ownerPhoneNum: number,
  ownerWhatsapp: number,
  price: string,
  key: string,
  downloadUrls: string[]



}
@Injectable({
  providedIn: 'root'
})
export class AddToFirebaseService {

  private adsCollection: AngularFirestoreCollection<Ads>;
  private ads: Observable<Ads[]>
  adscoll = 'AdsColl'

  constructor(
    private db: AngularFirestore,
    private afAuth: AngularFireAuth) {

    this.adsCollection = db.collection<Ads>('AdsColl');

    this.ads = this.adsCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      })
    )
  }

  getAllAds() {
    return this.ads;
  };

  getAdToUpdate(id: string) {
    return this.db.doc<any>(`${this.adscoll}/${id}`);
  }

  updateAds(content) {
    return this.getAdToUpdate(content.id).update({
      adTopic: content.adTopic,
      description: content.description,
      ownerPhoneNum: content.ownerPhoneNum,
      ownerWhatsapp: content.ownerWhatsapp,
      price: content.price
    })

  }




  addNewAd(ads: Ads): Promise<void> {
    const id = this.db.createId();
    ads.id = id;
    return this.db.doc(`AdsColl/${id}`).set(ads);
  }



  deleteAd(id: string): Promise<void> {
    return this.adsCollection.doc(id).delete();
  };

}
