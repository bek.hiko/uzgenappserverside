import { AddNewAdsPageModule } from './pages/add-new-ads/add-new-ads.module';
import { LoginPagePage } from './pages/login-page/login-page.page';
import { environment } from './../environments/environment';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireAuthModule } from '@Angular/fire/auth'; 
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule} from '@ionic/storage';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AngularFirestoreModule, FirestoreSettingsToken } from '@angular/fire/firestore';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import * as firebase from 'firebase/app';
import { AngularFireModule } from '@angular/fire';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { TaksiComponent } from './pages/taksi/taksi.component';

firebase.initializeApp(environment.firebase);
@NgModule({
  declarations: [AppComponent,TaksiComponent],
  entryComponents: [],
  imports: [BrowserModule,
     IonicModule.forRoot(),
      AppRoutingModule,
      AngularFireModule.initializeApp(environment.firebase),
      AngularFirestoreModule,
      ReactiveFormsModule,
      FormsModule,
      AddNewAdsPageModule,
      IonicStorageModule.forRoot()
  ],
  providers: [
    LoginPagePage,
    StatusBar,
    SplashScreen,
    // Storage
    WebView,
    AngularFireAuthModule,
    AngularFireAuth,
    AddNewAdsPageModule,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
