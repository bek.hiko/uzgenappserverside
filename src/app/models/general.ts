export class General {
  id: string;
  title: string;
  price: number;
  location: string;
  contactNumber: string;
  whatsAppNumber:string;
  createdAt =  new Date();
  updatedAt = new Date();
  photoUrls: string[] = [];
  description: string;
  views = 0;
  like =0;
  authorNick: string;
  ownerData: string;
  type:string;
  category = 'general'
}
