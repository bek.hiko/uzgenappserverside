export class User {
  id?: string;
  name: string;
  surname: string;
  email: string;
  phone?: string;
  nickname: string
  isTaksi = false;
}
