export class Taksi {
    id: string;
    contactNumber: string;
    location: string;
    createdAt =  new Date();
    updatedAt = new Date();
    carModel : string;
    taksiName : string;
    active = false;
    isTaksi :string;
    email:string;
}
