export class Posilka {
    id: string;
    contactNumber: string;
    whatsAppNumber:string;
    location: string;
    createdAt =  new Date();
    updatedAt = new Date();
    carModel : string;
    taksiName : string;
    description : string;
    finishDate : string;
}
