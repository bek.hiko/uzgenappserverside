export class Work {
  id?: string;
  title:string;
  type: string;
  price: number;
  location: string;
  contactNumber: string;
  whatsAppNumber:string;
  createdAt =  new Date();
  updatedAt = new Date();
  photoUrls: string[] = [];
  description: string;
  views = 0;
  authorNick: string;
  ownerData: string;
  category = 'work';
}
