export class Estate {
    id: string;
    title: string;
    rooms: number;
    type: string;
    area: number;
    price: number;
    location: string;
    contactNumber: string;
    whatsAppNumber:string;
    createdAt =  new Date();
    updatedAt = new Date();
    photoUrls: string[] = [];
    description: string;
    views = 0;
    like =0;
    authorNick: string;
    ownerData:string;
    category = 'estate';
}
