export class Meal {
  id: string;
  title: string;
  price: number;
  createdAt =  new Date();
  updatedAt = new Date();
  photoUrls: string[] = [];
  description: string;
}
