import { Meal } from './meal';

export class Restaurant {
  id: string;
  title: string;
  location: string;
  contactNumber: string;
  createdAt =  new Date();
  updatedAt = new Date();
  photoUrls: string[] = [];
  description: string;
  views = 0;
  like= 0;
  meals:Meal[] = [];
  authorNick: string;
  
}
