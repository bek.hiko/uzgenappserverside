export class Animals {
    id:string;
    title:string;
    type:string;
    color:string;
    price:string;
    quantity = 0;
    contactNumber: string;
    whatsAppNumber:string;
    location:string;
    createAt =new Date();
    updatedAt = new Date();
    photoUrls: string[] =[];
    description:string;
    views = 0;
    like =0;
    authorNick:string;
    ownerData:string;
}
