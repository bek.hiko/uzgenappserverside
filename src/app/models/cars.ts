export class Cars {
  id: string;
  title: string;
  mark: string;
  model: string;
  year: string;
  price: number;
  location: string;
  contactNumber: string;
  whatsAppNumber: string;
  createdAt =  new Date();
  updatedAt = new Date();
  photoUrls: string[] = [];
  description: string;
  views = 0;
  like =0;
  authorNick: string;
  ownerData: string;
  category = 'cars';
}
