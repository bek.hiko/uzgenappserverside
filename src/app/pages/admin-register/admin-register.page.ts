import { AdminAuthService } from './../../services/admin-auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';

export type EditorType = 'taksi' | 'admin';
@Component({
  selector: 'app-admin-register',
  templateUrl: './admin-register.page.html',
  styleUrls: ['./admin-register.page.scss'],
})
export class AdminRegisterPage implements OnInit {
  validations_form: FormGroup;
  validations_form_taksi: FormGroup;
  errorMessage: string = "";
  successMessage: string = "";

  validation_messages = {
    'email': [
      { type: "required", message: "Email is required" },
      { type: "pattern", message: "Please enter a valid email" },
    ],
    'password': [
      { type: "required", message: "Password is required" },
      { type: "minLength", message: "Password must be at least 6 characters" },
    ]
  };
  adminNick ='';
  disablePrevBtn = true;
  disableNextBtn = false;
  editor: EditorType = 'admin';

  constructor(
    public toastCtrl: ToastController,
    public authService:AdminAuthService,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private router: Router,
    private route : ActivatedRoute) {
      this.route.paramMap.subscribe(params =>{
        this.adminNick = params.get('adminNickName')
      })
     }
 
  ngOnInit() {
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])),
      name: new FormControl(''),
      surname: new FormControl(''),
      nickname: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ]))
    })

    this.validations_form_taksi = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])),
      carModel: new FormControl(''),
      name: new FormControl(''),
      location : new FormControl(''),
      isTaksist: new FormControl(''),
      contactNumber: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4)
      ]))
    })

  }

  checkType(event) {
    this.editor = event.target.value;
  }

  get isAdmin(){
    return this.editor === 'admin';
  }

  get isTaksi(){
    return this.editor === 'taksi';
  }

  tryRegister(value) {
    this.authService.registerUser(value).then(
      res => {
        let alertOptions = {
          header: "Account created Successfully",
          message: "New admin account is created \xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0\xa0 succesfully <br><br><b>Email: </b> " + value.email +
                     "<br><br><b>Password: </b>"+ value.password,
          buttons: ['OK']
        }
        this.showAlert(alertOptions);
        this.router.navigateByUrl(`/admin-page/${this.adminNick}`)

      }, err => {
        let alertOptions = {
          header: "Cannot register",
          message: err.message,
          buttons: ['ok']
        };
        this.showAlert(alertOptions);
      }
    )
  }
  async showAlert(options) {
    let alertDialog = await this.alertCtrl.create(options);
    return await alertDialog.present();
  }


}
