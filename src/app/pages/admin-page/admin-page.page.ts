import { GeneralService } from './../../services/general.service';
import { AnimalsService } from './../../services/animals.service';
import { RestuarantService } from './../../services/restuarant.service';
import { EstateService } from './../../services/estate.service';
import { UslugiService } from './../../services/uslugi.service';
import { ElectronicsService } from './../../services/electronics.service';
import { WorkService } from './../../services/work.service';
import { CarsService } from './../../services/cars.service';
import { General } from './../../models/general';
import { Animals } from './../../models/animals';
import { Restaurant } from './../../models/restaurant';
import { Estate } from './../../models/estate';
import { Electronics } from './../../models/electronics';
import { Cars } from './../../models/cars';
import { User } from './../../models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { AdminAuthService, Admin } from './../../services/admin-auth.service';
import { Observable } from 'rxjs';
import { AddToFirebaseService, Ads } from './../../services/add-to-firebase.service';

import { Component, OnInit, ViewChild } from '@angular/core';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { AngularFirestore } from '@angular/fire/firestore';
import { IonSlides, AlertController } from '@ionic/angular';


@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.page.html',
  styleUrls: ['./admin-page.page.scss'],
})
export class AdminPagePage implements OnInit {
  @ViewChild('slides', { static: false }) ionSlides: IonSlides

  public myads: Observable<Ads[]>
  public ads: Observable<any>;
  public singleAdminAds: Observable<any>;
  public allAdmins: Observable<any>;

  currentAdmin = new User();
  isSuperAdmin: boolean = false;
  adminNick: string = "";
  id: string = "";

  transport: Observable<any>;
  work:  Observable<any>;
  electronics: Observable<any>;
  uslugi: Observable<any>;
  estate: Observable<any>;
  restuarant: Observable<any>;
  animal: Observable<any>;
  general: Observable<any>;

  constructor(
    private addFirebaseService: AddToFirebaseService,
    private storage: Storage,
    private adminAuth: AdminAuthService,
    private db: AngularFirestore,
    private router: Router,
    private alertCtrl: AlertController,
    private route: ActivatedRoute,
    private transportService: CarsService,
    private workService: WorkService,
    private electronicsService: ElectronicsService,
    private uslugiService: UslugiService,
    private estateService: EstateService,
    private restuarantService: RestuarantService,
    private animalService: AnimalsService,
    private generalService: GeneralService
  ) {
    this.route.paramMap.subscribe(params => {
      this.adminNick = params.get('adminNickName')
      if (this.adminNick == "hiko9991" || this.adminNick == "ilhom8991") {
        this.isSuperAdmin = true;
        this.allAdmins = this.getAllAdmin();
      }
      console.log("Admin nick: ", this.adminNick)
      this.work =this.workService.getMyAds(this.adminNick);
      console.log(this.work)
      this.transport= this.transportService.getMyAds(this.adminNick);
      this.electronics= this.electronicsService.getMyAds(this.adminNick);
      this.uslugi= this.uslugiService.getMyAds(this.adminNick);
      this.estate =this.estateService.getMyAds(this.adminNick);
      this.restuarant=this.restuarantService.getMyAds(this.adminNick)
      this.animal =this.animalService.getMyAds(this.adminNick)
      this.general=this.generalService.getMyAds(this.adminNick)

    });

    this.storage.get('CurrentAdminInfo').then(adminInfo => {
      this.currentAdmin = adminInfo;
    })

  }

  ngOnInit() {

  }

  getAllAdmin() {
    return this.db.collection('AdminColl').valueChanges();
  }

  getMyads(adminNick: string) {
    this.work =this.workService.getMyAds(adminNick);
    this.transport= this.transportService.getMyAds(adminNick);
    this.electronics= this.electronicsService.getMyAds(adminNick);
    this.uslugi= this.uslugiService.getMyAds(adminNick);
    this.estate =this.estateService.getMyAds(adminNick);
    this.restuarant=this.restuarantService.getMyAds(adminNick)
    this.animal =this.animalService.getMyAds(adminNick)
    this.general=this.generalService.getMyAds(adminNick)
  }

  deleteSingleAd(id: string, category: string) {
    // this.addFirebaseService.deleteAd(id).then(() => {
    //   console.log("Ad deleted Successfully")
    // })
    switch (category) {
      case 'transport':
        this.transportService.deleteAd(id);
        break
      case 'work':
        this.workService.deleteAd(id)
        break
      case 'uslugi':
        this.uslugiService.deleteAd(id);
        break
      case 'electronics':
        this.electronicsService.deleteAd(id);
        break
      case 'estate':
        this.estateService.deleteAd(id);
        break
      case 'restuarant':
        this.restuarantService.deleteAd(id);
        break
      case 'animal':
        this.animalService.deleteAd(id)
        break
      case 'general':
        this.generalService.deleteAd(id)
        break
    }
  }

  async presentAlertConfirmation(id ,category) {
    let alert = await this.alertCtrl.create({
      header: 'Удаления обьевлении',
      message: 'При удаления она будет без возвратно удалена из база данных.',
      buttons: [
        {
          text: 'Отмена',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Удалить',
          handler: () => {
            this.deleteSingleAd(id,category)
          }
        }
      ]
    });
    alert.present();
  }

  logOut() {
    this.storage.clear();
    this.adminAuth.logout();
    this.router.navigate(['/login-page']);
    //location.reload()
  }

}
