import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleAdPage } from './single-ad.page';

const routes: Routes = [
  {
    path: '',
    component: SingleAdPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingleAdPageRoutingModule {}
