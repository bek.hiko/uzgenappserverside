import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SingleAdPage } from './single-ad.page';

describe('SingleAdPage', () => {
  let component: SingleAdPage;
  let fixture: ComponentFixture<SingleAdPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleAdPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SingleAdPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
