import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SingleAdPageRoutingModule } from './single-ad-routing.module';

import { SingleAdPage } from './single-ad.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SingleAdPageRoutingModule
  ],
  declarations: [SingleAdPage]
})
export class SingleAdPageModule {}
