import { Observable } from 'rxjs';
import { AddToFirebaseService ,Ads} from './../../services/add-to-firebase.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-single-ad',
  templateUrl: './single-ad.page.html',
  styleUrls: ['./single-ad.page.scss'],
})
export class SingleAdPage implements OnInit {
  public currentAd:Observable<any>;
  
  currentAdID ="";
  constructor(private activatedRoute:ActivatedRoute,
              private addFirebase: AddToFirebaseService,
              private db:AngularFirestore) { }

  ngOnInit() {
    this.currentAdID = this.activatedRoute.snapshot.params['id'];

    this.currentAd = this.db.collection('AdsColl',
    ref => ref.where('id', '==', this.currentAdID)).valueChanges();

    console.log(this.currentAd)
    
  }

}
