import { AnimalsService } from './../../../services/animals.service';
import { WorkService } from './../../../services/work.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { ImagePicker} from '@ionic-native/image-picker/ngx';
import { Work } from 'src/app/models/work';
import { ActivatedRoute, Router } from '@angular/router';
import { Animals } from 'src/app/models/animals';

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.scss'],
})
export class AnimalComponent implements OnInit {
  createNewWork = new Animals;
  adminNick =''
  constructor(
    public imagePicker: ImagePicker,
    private webView: WebView,
    private route: ActivatedRoute,
    private workService: AnimalsService,
    private router: Router) {
      this.route.paramMap.subscribe(params =>{
        this.adminNick = params.get('adminNickName')
        this.createNewWork.authorNick = this.adminNick;
      })
     }

  ngOnInit() {}

  selectedImages: any[] =[]
  image = [];
  async openImagePicker() {
    const permissionResult = await this.imagePicker.hasReadPermission();
    if (permissionResult == false) {
      // no callbacks required as this opens a popup which returns async
      await this.imagePicker.requestReadPermission();
    }

    if (permissionResult == true) {
      try {
        const selectedImages = await this.imagePicker.getPictures({
          maximumImagesCount: 8,
          quality: 25
        });
        this.selectedImages = selectedImages;
      } catch (err) {
        console.log(err);
      }
    }
    console.log("Image is Successfully picked from gallery");
  }
  
  encodeImageUri(imageUri): Promise<string> {
    return new Promise((resolve, reject) => {
      var c = document.createElement('canvas');
      var ctx = c.getContext("2d");
      var img = new Image();
      img.onload = function () {
        var aux: any = this;
        c.width = aux.width;
        c.height = aux.height;
        ctx.drawImage(img, 0, 0);
        var dataURL = c.toDataURL("image/jpeg");
        resolve(dataURL);
      };
      img.src = imageUri;
    });
  };

  async createNew(){

    for (let i = 0; i < this.selectedImages.length; i++) {
      console.log(this.selectedImages[i]);
      let imageUrlFirst = this.webView.convertFileSrc(this.selectedImages[i]);
      console.log(imageUrlFirst);
      let imageUrl = await this.encodeImageUri(imageUrlFirst);
      let storageRef = firebase.storage().ref();
      let imageRef = storageRef.child('image').child(`${new Date().getTime()}`);
      let uploadRes = await imageRef.putString(imageUrl, 'data_url')
      console.log("uploaded image");
      console.log(uploadRes);
      console.log(imageRef.getDownloadURL)
      let downloadimageUrl = await imageRef.getDownloadURL();
      console.log("retrieved image url");
      console.log(downloadimageUrl);
      this.createNewWork.photoUrls.push(downloadimageUrl);

    }
    this.workService.addNewAnimal(this.createNewWork).then(()=>{
      this.router.navigateByUrl(`admin-page/${this.adminNick}`)
      console.log('Created New order')
    })
 }

}
