import { AddNewMealModalComponent } from './../add-new-meal-modal/add-new-meal-modal.component';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-restuarant',
  templateUrl: './restuarant.component.html',
  styleUrls: ['./restuarant.component.scss'],
})
export class RestuarantComponent implements OnInit {
  adminNick =''
  constructor( private route: ActivatedRoute, private modalCtrl: ModalController) {
    this.route.paramMap.subscribe(params =>{
      this.adminNick = params.get('adminNickName');
    })
   }

  ngOnInit() {}
  async presentModal() {
    const modal = await this.modalCtrl.create({
      component: AddNewMealModalComponent,
      cssClass: 'select-modal'
      });
    return await modal.present();
  }
  
}
