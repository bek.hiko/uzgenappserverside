import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-choose-category',
  templateUrl: './choose-category.component.html',
  styleUrls: ['./choose-category.component.scss'],
})
export class ChooseCategoryComponent implements OnInit {
  adminNick: string
  constructor( private route: ActivatedRoute) {
        this.route.paramMap.subscribe(params =>{
          this.adminNick =params.get('adminNickName')
        })
   }

  ngOnInit() {}

}
