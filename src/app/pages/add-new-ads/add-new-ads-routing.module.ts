import { UslugiComponent } from './uslugi/uslugi.component';
import { ChooseCategoryComponent } from './choose-category/choose-category.component';
import { WorkComponent } from './work/work.component';
import { TransportComponent } from './transport/transport.component';
import { RestuarantComponent } from './restuarant/restuarant.component';
import { GeneralComponent } from './general/general.component';
import { EstateComponent } from './estate/estate.component';
import { ElectronicComponent } from './electronic/electronic.component';
import { AnimalComponent } from './animal/animal.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path : 'addnew/animal:adminNickName',
    component: AnimalComponent
  },
  {
    path: 'addnew/electronic/:adminNickName',
    component: ElectronicComponent
  },
  {
    path: 'addnew/estate/:adminNickName',
    component: EstateComponent
  },
  {
    path: 'addnew/general/:adminNickName',
    component: GeneralComponent
  },
  {
    path: 'addnew/restuarant/:adminNickName',
    component: RestuarantComponent
  },
  {
    path:'addnew/transport/:adminNickName',
    component:TransportComponent
  },
  {
    path: 'addnew/work/:adminNickName',
    component: WorkComponent
  },
  {
    path:'chooseCategory/:adminNickName',
    component:ChooseCategoryComponent
  },
  {
    path:'addnew/uslugi/:adminNickName',
    component:UslugiComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddNewAdsPageRoutingModule {}
