import { AddNewMealModalComponent } from './add-new-meal-modal/add-new-meal-modal.component';
import { UslugiComponent } from './uslugi/uslugi.component';
import { ChooseCategoryComponent } from './choose-category/choose-category.component';
import { WorkComponent } from './work/work.component';
import { TransportComponent } from './transport/transport.component';
import { RestuarantComponent } from './restuarant/restuarant.component';
import { GeneralComponent } from './general/general.component';
import { EstateComponent } from './estate/estate.component';
import { ElectronicComponent } from './electronic/electronic.component';
import { AnimalComponent } from './animal/animal.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AddNewAdsPageRoutingModule } from './add-new-ads-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddNewAdsPageRoutingModule
  ],
  entryComponents: [AddNewMealModalComponent],

  providers: [ImagePicker],
  declarations: [
    AnimalComponent,
    ElectronicComponent,
    EstateComponent,
    GeneralComponent,
    RestuarantComponent,
    TransportComponent,
    WorkComponent,
    ChooseCategoryComponent,
    UslugiComponent,
    AddNewMealModalComponent
    ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AddNewAdsPageModule { }
