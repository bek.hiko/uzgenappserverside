import { Meal } from './../../../models/meal';
import { MealStorageService } from './../../../services/meal-storage.service';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-new-meal-modal',
  templateUrl: './add-new-meal-modal.component.html',
  styleUrls: ['./add-new-meal-modal.component.scss'],
})
export class AddNewMealModalComponent implements OnInit {

  constructor(private modalCtrl: ModalController, private storage:Storage, private mealStorage: MealStorageService) { }

  item = []
  ngOnInit() {}

  dismissModal() {
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  addtostorage(){
    let item =[]
    let hiko = new Meal
    this.mealStorage.saveMealAsArray(hiko)
  }

  getData(){
    this.mealStorage.getMealArray().then(res =>{
          this.item = res
          console.log('res', res)
    })
  }

}
