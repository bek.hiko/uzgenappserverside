import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireAuthModule } from '@Angular/fire/auth'; 
import { IonicModule } from '@ionic/angular';

import { IonicStorageModule } from '@ionic/storage';


import { LoginPagePageRoutingModule } from './login-page-routing.module';

import { ReactiveFormsModule } from '@angular/forms';
import { LoginPagePage } from './login-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    LoginPagePageRoutingModule,
    IonicStorageModule.forRoot()
  ],

  providers:[
    AngularFireAuthModule,
    AngularFireAuth,
    //Storage
  ],
  declarations: [LoginPagePage]
})
export class LoginPagePageModule {}
