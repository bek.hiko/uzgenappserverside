import { User } from './../../models/user';
import { AdminAuthService } from './../../services/admin-auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { NavController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
import { first } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

export type EditorType = 'taksi' | 'admin';
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.page.html',
  styleUrls: ['./login-page.page.scss'],
})
export class LoginPagePage implements OnInit {
  public isLoggedin: boolean = false;
  validations_form: FormGroup;
  errorMessage: string = "";
  currentAdmin: any;
  id: string = "";
  adminNicname = '';
  taksName ='';
  editor: EditorType = 'admin';
  constructor(
    public afAuth: AngularFireAuth,
    private adminAuth: AdminAuthService,
    private router: Router,
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private formBuilder: FormBuilder,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.storage.get('CurrentAdminInfo').then(user =>{
        if(user != null && !user.isTaksi){
          this.adminNicname =user.nickname;
          this.router.navigateByUrl(`/admin-page/${this.adminNicname}`)
        }else if(user != null && user.isTaksi){
          this.router.navigateByUrl(`/taksi/${user.taksiName}`)
        }
    })
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ]))
    });
  }

  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required' },
      { type: 'pattern', message: 'Please enter a valid email address' },
    ],
    'password': [
      { type: 'required', message: 'You must provide a password' },
      { type: 'minlength', message: 'The minimum length of your password must be 6' },
    ]
  }

  checkType(event) {
    this.editor = event.target.value;
  }

  get isAdmin(){
    return this.editor === 'admin';
  }

  get isTaksi(){
    return this.editor === 'taksi';
  }
  async login(value) {
    await this.adminAuth.login(value).then(res => {
      firebase
        .firestore()
        .doc(`/AdminColl/${res.user.uid}`)
        .get()
        .then(userProfileSnapshot => {
          this.id = userProfileSnapshot.data().id;
          this.adminAuth.getCurrentAdminInfo(this.id).subscribe(admin => {
            this.currentAdmin = admin;
            this.storage.set('CurrentAdminInfo', this.currentAdmin);
            this.router.navigateByUrl(`/admin-page/${admin.nickname}`)
          })

        });

    }, err => {
      this.showAlert()
      console.log(err);
    })
  }

  async loginAsTaksi(value) {
    await this.adminAuth.login(value).then(res => {
      firebase
        .firestore()
        .doc(`/taksiColl/${res.user.uid}`)
        .get()
        .then(userProfileSnapshot => {
          this.id = userProfileSnapshot.data().id;
            this.router.navigateByUrl(`taksi/${this.id}`)
        });

    }, err => {
      this.showAlert()
      console.log(err);
    })
  }

  async showAlert() {
    let alert = await this.alertCtrl.create({
      header: 'Ошибка при входе',
      message: 'Не правильный пароль или логин',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


}
