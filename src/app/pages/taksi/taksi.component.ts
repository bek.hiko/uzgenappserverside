import { Taksi } from './../../models/taksi';
import { AlertController } from '@ionic/angular';
import { AdminAuthService } from './../../services/admin-auth.service';
import { Storage } from '@ionic/storage';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-taksi',
  templateUrl: './taksi.component.html',
  styleUrls: ['./taksi.component.scss'],
})
export class TaksiComponent implements OnInit {
  taksi = ''
  taksiUser:Taksi
  constructor(
    private storage: Storage,
    private adminAuth : AdminAuthService,
    private route:Router,
    private router:ActivatedRoute,
    private alert :AlertController) { 
    this.router.paramMap.subscribe(params =>{
      this.taksi =params.get('id');
      this.adminAuth.getCurrentTaksiInfo(this.taksi).subscribe(taksi =>{
        this.taksiUser = taksi
      })
    })

    
  }

  ngOnInit() {}

  logOut() {
    this.storage.clear();
    this.adminAuth.logout();
    this.route.navigate(['/login-page']);
  }

  async submitWork(){
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: 'Хорошого отдыха',
      message: 'Сегодня вы поработали на славу хорошенко отдахните',
      backdropDismiss: true,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
         
        },
        {
          text: 'Cохранить',
          handler: () => {
            this.updateTaksi();
            setTimeout(() => {
              alert.dismiss();
            }, 400)
          }
        }
      ]
    });

    await alert.present();

  }

  updateTaksi(){
    this.taksiUser.active = !this.taksiUser.active
    const work = this.taksiUser.active
    this.adminAuth.updateTaksiInfo(this.taksi, work);
  }

}
