import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TaksiComponent } from './taksi.component';

describe('TaksiComponent', () => {
  let component: TaksiComponent;
  let fixture: ComponentFixture<TaksiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaksiComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TaksiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
