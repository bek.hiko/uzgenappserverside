import { TaksiComponent } from './pages/taksi/taksi.component';

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login-page',
    pathMatch: 'full'
  },
  {
    path: 'admin-page/:adminNickName',
    loadChildren: () => import('./pages/admin-page/admin-page.module').then(m => m.AdminPagePageModule)
  },
  {
    path: 'login-page',
    loadChildren: () => import('./pages/login-page/login-page.module').then(m => m.LoginPagePageModule)
  },
  {
    path: 'admin-register/:adminNickName',
    loadChildren: () => import('./pages/admin-register/admin-register.module').then(m => m.AdminRegisterPageModule)
  },
  {
    path: 'taksi/:id',
    component: TaksiComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
